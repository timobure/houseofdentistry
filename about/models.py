from django.db import models


class About(models.Model):
    heading = models.CharField(max_length=25)
    image_1 = models.ImageField(upload_to='about/about/')
    image_2 = models.ImageField(upload_to='about/about/')
    content = models.TextField()

    class Meta:
        verbose_name = 'About Us'
        verbose_name_plural = 'About Us'


class ReasonToChooseUs(models.Model):
    heading = models.CharField(max_length=25, unique=True)
    image = models.ImageField(upload_to='about/reasons/')
    content = models.TextField()

    class Meta:
        verbose_name = 'Why Choose Us'
        verbose_name_plural = 'Why Choose Us'

    
