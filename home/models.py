from django.db import models


class SlideShow(models.Model):
    slide_no = models.IntegerField()
    heading = models.TextField()
    sub_heading = models.TextField()
    image = models.ImageField(upload_to='home/sliders/')
    publish = models.BooleanField(default=False)


    class Meta:
        verbose_name = 'Slide Show'
        verbose_name_plural = 'Slide Show'

    def __str__(self):
        return f'Slide {self.slide_no}'


class HelpBox(models.Model):
    heading = models.CharField(max_length=25, unique=True)
    content = models.TextField() 

    class Meta:
        verbose_name = 'Help Box'
        verbose_name_plural = 'Help Boxes'

    def __str__(self):
        return self.heading


class OpeningHours(models.Model):
    period = models.CharField(max_length=25, unique=True)
    opening = models.TimeField()
    closing = models.TimeField()

    class Meta:
        verbose_name = 'Opening Hours'
        verbose_name_plural = 'Opening Hours'

    def __str__(self):
        return self.period


class WelcomeMessage(models.Model):
    heading = models.CharField(max_length=25)
    content = models.TextField()

    class Meta:
        verbose_name = 'Welcome Message'
        verbose_name_plural = 'Welcome Message'

    def __str__(self):
        return self.heading




