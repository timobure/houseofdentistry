# from django.urls import nko
from django.urls import path
from . import views


app_name = "patients"

urlpatterns = [
    path('', views.PatientsPage.as_view(), name="patients")
]

