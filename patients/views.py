from django.views.generic import TemplateView


class PatientsPage(TemplateView):
    template_name = "patients/patient.html"