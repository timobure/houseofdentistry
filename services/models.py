from django.db import models


class ReasonToChooseUs(models.Model):
    heading = models.CharField(max_length=25, unique=True)
    image = models.ImageField(upload_to='about/reasons/')
    content = models.TextField()

    class Meta:
        verbose_name = 'Why Choose Us'
        verbose_name_plural = 'Why Choose Us'
