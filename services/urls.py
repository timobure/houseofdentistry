from django.urls import path
from . import views

app_name = "services"

urlpatterns = [
    path('', views.ServicesPage.as_view(), name="services"),
    path('<slug:slug>/', views.ServicesPage.as_view(), name="service")
]

